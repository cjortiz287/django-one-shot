from django.urls import path
from . import views
from .views import todo_list_view

urlpatterns = [
    path('<int:id>/edit/', views.todo_list_update_view, name='todo_list_update'),
    path('<int:id>/delete/', views.todo_list_delete_view, name='todo_list_delete'),
    path('<int:id/', views.todo_list_detail, name='todo_list_detail'),
    path('create', views.TodoListCreateView.as_view(), name='todo_list_create'),
    path('items/create/', views.todo_item_create_view, name='todo_item_create'),

]
