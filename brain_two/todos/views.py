from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from .models import TodoList
from .forms import TodoListForm

# Create your views here.

def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_list': todo_lists
    }
    return render(request, 'todo/todo_list.html', context)

def todo_list_update_view(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = TodoListForm(instance=todo_list)

    return render(request, 'todos/todo_list_update.html', {'form': form})

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    return render(request, 'todos/todo_list_detail.html', {'todo_list': todo_list})

class TodoListCreateView(View):
    def get(self, request):
        form = TodoListForm()
        return render(request, 'todo/todo_list_create.html', {'form': form})

    def post(self, request):
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_todo_list = form.save()
            return redirect('todo_list_detail', id=new_todo_list.id)
        return render(request, 'todos/todo_list_create.html', {'form': form})

def todo_list_delete_view(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        todo_list.delete()
        return redirect('todo_list_view')

    return render(request, 'todos/todo_list_delete.html', {'todo_list': todo_list})

def todo_item_create_view(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_todo_item = form.save()
            return redirect('todo_list_detail', id=new_todo_item.list.id)
    else:
        form = TodoItemForm()

    return render(request, 'todos/todo_item_create.html', {'form': form})
